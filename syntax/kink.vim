" Vim syntax file
" Language:	Kink (https://bitbucket.org/kink/kink/src)
" Maintainer:	Miyakawa Taku <miyakawa.taku@gmail.com>
" Last Change:	2018-12-24

" Copyright (c) 2013- Miyakawa Taku
" 
" Permission is hereby granted, free of charge, to any person obtaining a copy
" of this software and associated documentation files (the "Software"), to deal
" in the Software without restriction, including without limitation the rights
" to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
" copies of the Software, and to permit persons to whom the Software is
" furnished to do so, subject to the following conditions:
" 
" The above copyright notice and this permission notice shall be included in
" all copies or substantial portions of the Software.
" 
" THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
" IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
" FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
" AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
" LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
" OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
" THE SOFTWARE.

" Quit when a syntax file was already loaded
if exists("b:current_syntax")
  finish
endif

let s:save_cpo = &cpo
set cpo&vim

syntax match kinkVerbRef ":\s*[a-z_][a-z_0-9?]*"
syntax match kinkVerbDeref "\$\s*[a-z_][a-z_0-9?]*"
syntax match kinkVerbCall "[a-z_][a-z_0-9?]*"

syntax match kinkNounRef ":\s*\([a-z_][a-z_0-9?]*\)\?[A-Z][a-zA-Z_0-9?]*"
syntax match kinkNounDeref "\([a-z_][a-z_0-9?]*\)\?[A-Z][a-zA-Z_0-9?]*"

syntax keyword kinkTodo contained TODO FIXME XXX
syntax match kinkComment "#.*" contains=kinkTodo
syntax region kinkString skip=+''+ matchgroup=kinkQuoteStart start=+'+ matchgroup=kinkQuoteEnd end=+'+
syntax region kinkString skip=+\\.+ matchgroup=kinkQuoteStart start=+"+ matchgroup=kinkQuoteEnd end=+"+ contains=kinkStringEscape
syntax region kinkString matchgroup=kinkQuoteStart start=+(\z(=\+\)'+ matchgroup=kinkQuoteEnd end=+'\z1)+
syntax match kinkStringEscape contained +\\[0tnrabefv"\\]\|\\u[0-9a-f]\{4}\|\\x{[0-9a-f]\{1,6}}+
syntax match kinkPseudoVariable "\\\(env\|recv\|args\|[0-9][0-9_]*\|0x[0-9a-f_]*\|0b[01_]*\)\>"
syntax match kinkInteger "0x[0-9a-f_]*\|0b[01_]*\|[0-9][0-9_]*"
syntax match kinkDecimal "[0-9][0-9_]*\.[0-9][0-9_]*"

" Define the default highlighting.
highlight default link kinkVerbRef Identifier
highlight default link kinkVerbDeref Identifier
highlight default link kinkNounRef Identifier
highlight default link kinkNounDeref Identifier
highlight default link kinkComment Comment
highlight default link kinkTodo Todo
highlight default link kinkString String
highlight default link kinkQuoteStart String
highlight default link kinkQuoteEnd String
highlight default link kinkStringEscape SpecialChar
highlight default link kinkPseudoVariable Special
highlight default link kinkInteger Number
highlight default link kinkDecimal Float

let b:current_syntax = "kink"

let &cpo = s:save_cpo
unlet s:save_cpo

" vim: et sw=2 sts=2

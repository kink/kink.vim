kink.vim: Vim Plugins for Kink Programming Language
===================================================

Synopsis
--------
Vim plugins (ftdetect, ftplugin, indent, plugin, syntax)
for Kink Programming Language (https://osdn.net/projects/kink/).

Install
-------
If you are using dein, add this line into your .vimrc.

::

    call dein#add('https://scm.osdn.net/gitroot/kink/kink.vim.git')

Then execute ``:call dein#install()``.

.. vim: et sw=4 sts=4

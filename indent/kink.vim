" Vim indent file for Kink
" Language:	Kink (https://bitbucket.org/kink/kink)
" Maintainer:	Miyakawa Taku <miyakawa.taku@gmail.com>
" Last Change:  2018-12-24

" Copyright (c) 2013- Miyakawa Taku
" 
" Permission is hereby granted, free of charge, to any person obtaining a copy
" of this software and associated documentation files (the "Software"), to deal
" in the Software without restriction, including without limitation the rights
" to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
" copies of the Software, and to permit persons to whom the Software is
" furnished to do so, subject to the following conditions:
" 
" The above copyright notice and this permission notice shall be included in
" all copies or substantial portions of the Software.
" 
" THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
" IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
" FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
" AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
" LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
" OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
" THE SOFTWARE.

" Loads the script when no other was loaded
if exists("b:did_indent")
  finish
endif
let b:did_indent = 1

" Basic settings
setlocal nolisp
setlocal autoindent

setlocal indentexpr=GetKinkIndent(v:lnum)
setlocal indentkeys=0},0),0=],!^F,o,O

let s:save_cpo = &cpo
set cpo&vim

let s:skip = 'has("syntax_items") && synIDattr(synID(line("."), col("."), 1), "name") =~ "String\\|Quote\\|Comment"'

" Returns the indent
function! GetKinkIndent(line_no)
  let prev_line_no = prevnonblank(a:line_no - 1)
  if prev_line_no == 0 || s:head_in_string(a:line_no)
    return -1
  endif

  if getline(a:line_no) =~ '^\s*[\]})]'
    " The current line is closing a brace/paren/bracket
    let [open_line_no, open_col_no] = s:get_opener_pos_for_first_closer_in_line(a:line_no)
    if open_line_no <= 0
      " No corresponding opener
      return -1
    else
      " Indent based on the opener
      let [ignored, base_indent] = s:get_base_lineno_and_indent(open_line_no)
      let remaining_openers = s:get_remaining_openers(open_line_no, open_col_no)
      return base_indent + &shiftwidth * remaining_openers
    endif
  else
    " Indent based on the previous line
    let [base_lineno, base_indent] = s:get_base_lineno_and_indent(prev_line_no)
    let openers = s:get_openers_till(base_lineno, a:line_no)
    return base_indent + &shiftwidth * openers
  endif
endfunction

" Returns the position of the opener corresponding to the
" opener at the head of the current line.
" Or returns [0, 0] if such an opener does not exist.
function! s:get_opener_pos_for_first_closer_in_line(line_no)
  let closing = matchstr(getline(a:line_no), '[\]})]')
  let [op_pattern, cl_pattern] = s:patterns_for_closing(closing)
  call cursor(a:line_no, 1)
  return searchpairpos(op_pattern, '', cl_pattern, 'bnW', s:skip)
endfunction

" Returns the opener-closer patterns pair for the character of the opener.
function! s:patterns_for_opening(opening)
  let op_pattern = (a:opening == '[' ? '\[' : a:opening)
  let cl_pattern = (a:opening == '{' ? '}' : a:opening == '[' ? ']' : ')')
  return [op_pattern, cl_pattern]
endfunction

" Returns the opener-closer patterns pair for the character of the closer.
function! s:patterns_for_closing(closing)
  let op_pattern = (a:closing == '}' ? '{' : a:closing == ']' ? '\[' : '(')
  return [op_pattern, a:closing]
endfunction

" Returns [base_lineno, base_indent].
function! s:get_base_lineno_and_indent(line_no)
  call cursor(a:line_no, 1)
  call cursor(a:line_no, col('$'))
  let ln = a:line_no
  let search_option = 'cbW'
  while search('[\]})]', search_option, a:line_no)
    let search_option = 'bW'
    if s:in_comment_or_string()
      continue
    endif

    let closing = s:get_current_char()
    let [op_pattern, cl_pattern] = s:patterns_for_closing(closing)
    let [open_lineno, open_colno] = searchpairpos(op_pattern, '', cl_pattern, 'bW', s:skip)
    if open_lineno == 0
      " no corresponding opener
      break
    elseif open_lineno != a:line_no
      " corresponding opener in another line
      let [ignored, base_indent] = s:get_base_lineno_and_indent(open_lineno)
      let indent = base_indent + &shiftwidth * s:get_remaining_openers(open_lineno, open_colno)
      return [a:line_no, indent]
    endif
  endwhile

  if s:head_in_string(line('.'))
    return s:get_base_lineno_and_indent(a:line_no - 1)
  else
    return [a:line_no, indent(a:line_no)]
  endif
endfunction

" Returns the number of openers,
" which is opening till the specified line number.
function! s:get_openers_till(line_no, open_till_lineno)
  let openers = 0
  call cursor(a:line_no, 1)
  let search_option = 'cW'
  while search('[[{(]', search_option, a:open_till_lineno - 1)
    let search_option = 'W'
    if s:in_comment_or_string()
      continue
    endif

    let opening = s:get_current_char()
    let [op_pattern, cl_pattern] = s:patterns_for_opening(opening)
    let [closing_lineno, closing_colno] = searchpairpos(op_pattern, '', cl_pattern, 'nW', s:skip)
    if closing_lineno <= 0 || closing_lineno >= a:open_till_lineno
      let openers += 1
    else
      call cursor(closing_lineno, closing_colno)
    endif
  endwhile

  return openers
endfunction

" Retunrs the number of openers which is not closed within the line,
" and placed before the stop column.
function! s:get_remaining_openers(line_no, shown_before_colno)
  let openers = 0
  call cursor(a:line_no, 1)
  let search_option = 'cW'
  while search('[[{(]', search_option, a:line_no) > 0 && col('.') < a:shown_before_colno
    let search_option = 'W'
    if s:in_comment_or_string()
      continue
    endif

    let opening = s:get_current_char()
    let [op_pattern, cl_pattern] = s:patterns_for_opening(opening)
    let [closing_lineno, closing_colno] = searchpairpos(op_pattern, '', cl_pattern, 'nW', s:skip)
    if closing_lineno != a:line_no
      let openers += 1
    else
      call cursor(closing_lineno, closing_colno)
    endif
  endwhile

  return openers
endfunction

" Returns the character on the cursor.
function! s:get_current_char()
  return getline(line('.'))[col('.') - 1]
endfunction

" Returns true if the cursor is in a string or a comment.
function! s:in_comment_or_string()
  return has('syntax_items') && synIDattr(synID(line('.'), col('.'), 1), "name") =~ 'String\|Quote\|Comment'
endfunction

" Returns true if the line starts in a string.
function! s:head_in_string(lineno)
  if ! has('syntax_items')
    return 0
  endif

  if getline(a:lineno) != ''
    return synIDattr(synID(a:lineno, 1, 1), "name") =~ 'String\|QuoteEnd'
  end

  let ln = a:lineno
  while 1
    if ln == 0
      return 0
    endif

    if getline(ln) != ""
      break
    endif

    let ln = ln - 1
  endwhile

  call cursor(ln, 1)
  let endcol = col('$') - 1
  return synIDattr(synID(ln, endcol, 1), "name") =~ 'String\|QuoteStart'
endfunction

let &cpo = s:save_cpo
unlet s:save_cpo

" vim: et sw=2 sts=2
